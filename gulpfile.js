var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    normalize = require('node-normalize-scss'),
    jade = require('gulp-jade'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    smushit = require('gulp-smushit'),
    browserSync = require('browser-sync'),
    shell = require('gulp-shell');
    rename = require('gulp-rename');
    concat = require('gulp-concat');
    htmlmin = require('gulp-htmlmin');

gulp.task('browser-sync', ['scss'],  function() {
  browserSync.init({
    server: {
      baseDir: "./app/"
    }
  });
});


// Собираем scss
gulp.task('scss', function(){
  return gulp.src('./src/scss/**/*.scss')
    .pipe(
      sass({
        includePaths: normalize.with('./src/scss/', './src/scss/blocks/', './src/scss/mixins/')
      })
      .on('error', function(err){
        console.log(err)
      })
    )
    .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }))
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./app/css/'))
    .pipe(browserSync.reload({stream:true}))
});

// Собираем Jade
gulp.task('jade', function() {
    gulp.src('./src/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .on('error', function(err){
          console.log(err)
        })
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./app/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('reload', function () {
  browserSync.reload();
});

gulp.task('js', function () {
    gulp.src("src/js/**/*.js")
      // .pipe(concat('main.js'))
      .pipe(uglify())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('app/js/'))
      .pipe(browserSync.reload({stream:true}))
});

gulp.task('imagemin', function () {
    return gulp.src('src/images/**/*')
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()],
            optimizationLevel: 5,
            svgoPlugins: [{removeViewBox: true}]
        }))
        .pipe(gulp.dest('app/images/'))
        .pipe(browserSync.reload({stream:true}))
});

gulp.task('watch', function(){
  gulp.watch('./src/*.jade', ['jade']);
  gulp.watch('./src/scss/**/*.scss', ['scss']);
  gulp.watch('src/js/**/*.js', ['js']);
  gulp.watch('src/images/**/*', ['imagemin']);
  // gulp.watch('**/*.{php,inc,info,css}',['reload']);
});

gulp.task('default', ['browser-sync', 'watch']);
